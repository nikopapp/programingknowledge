// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {
    private ArrayList<String> strings = new ArrayList<String>();
    public int solution(String S, int K) {
        // write your code in Java SE 8
        if(getValidLength(S)==S.length()) return S.length();
        swap(S);
        if(K>1){
          for(int z=0;z<K-1;z++){
            for(int i=z*S.length();i<(z+1)*S.length();i++)
            swap(strings.get(i));
          }
        }
        // if(K>2){
        //   for(int i=S.length();i<S.length();i++)
        //     swap(strings.get(i));
        // }
        return getValidLength();

    }
    private void swap(String S){
      for(int i=0;i<S.length();i++){
        if(i==0) strings.add(swapChar(S.charAt(i))+S.substring(i+1));
        else{
          strings.add(S.substring(0,i)+swapChar(S.charAt(i))+S.substring(i+1));
        }
      }
    }
    private int getValidLength(){
      int tempLength=0,maxLength=0;
      for(String s:strings){
        tempLength=getValidLength(s);
        if(tempLength>maxLength) maxLength=tempLength;
      }
      return maxLength;
    }
    private int getValidLength(String S){
      if(isValid(S)==0) return S.length();
      for(int i=S.length()-2;i>=2;i-=2){
        for(int j=(-S.length()+i)/2;j<(S.length()-i)/2;j++){
          if(isValid(S.substring((S.length()-i)/2+j,j+S.length()-(S.length()-i)/2))==0){
            System.out.println(S.substring((S.length()-i)/2+j,j+S.length()-(S.length()-i)/2));
            return i;
          }
        }
      }
      return 0;
    }
    private char swapChar(char c){
        return c=='('?')':'(';
    }
    private int isValid(String S){
        int bracks = 0;
        if(S.charAt(0) ==')') return -1;
        else bracks++;
        if(S.charAt(S.length()-1) =='(') return -1;
        for (int i=1;i<S.length()-1;i++){
            if(S.charAt(i)=='(') bracks++;
            if(S.charAt(i)==')') bracks--;
            if(bracks<0) return -1;
        }
        return bracks-1;
    }
    public void test(){
      String[][] list = new String[10][3];
      list[0][0]="()))((()())(";
      list[0][1]="2";
      list[0][2]="12";
      list[1][0]="()()()((()()))";
      list[1][1]="0";
      list[1][2]="14";
      for(int i=0;i<2;i++)
        System.out.println(list[i][0]+" "+solution(list[i][0],Integer.parseInt(list[i][1]))+"expected"+list[i][2]);
          isValid("()((()))");
    }
}
