public class CodilityTest {
    public int solution(int[] A) {
        for(int i=0;i<A.length;i++){
            if(isEqui(A,i)) return i;
        }
        return -1;
    }
    private boolean isEqui(int[] A,int i){
        long sumRight=0,sumLeft=0;
        for(int j=i+1;j<A.length;j++){
            sumRight+=A[j];
        }
        for(int j=i-1;j>=0;j--){
            sumLeft+=A[j];
        }
        return(sumLeft==sumRight);
    }
    public void test(){
      int[][] A={
        {-1,-1,-1,1,1,1,1},
        {-1,0}
                };
      for(int i=0;i<2;i++){
        System.out.println(solution(A[i]));
      }
    }
}
