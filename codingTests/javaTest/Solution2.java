// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {
    ArrayList<String> strings = new ArrayList<String>();
    public int solution(String S, int K) {
        // write your code in Java SE 8
        if((getValidLength(S,0)==S.length())||K==0) return getValidLength(S,0);
        if(K==1) swap(S);
        if(K==2){
            swap(S);
            for(int i=0;i<S.length();i++)
                swap(strings.get(i));
        }
        int temp,maxLength=0;
        for(String s:strings){
            temp=getValidLength(s,0);
            System.out.println(s+" : "+temp);
          if(temp>maxLength) maxLength = temp;

        }
        return strings.size();

    }
    private void swap(String S){

        String Stemp;
        for(int k=0;k<S.length();k++){
            if(k==0) {
                Stemp = ""+swapChar(S.charAt(0))+S.substring(1);
                    strings.add(Stemp);
                }else{
                    strings.add(S.substring(0,k)+swapChar(S.charAt(k))+S.substring(k+1));
                }
            }
    }
    private int getValidLength(String S,int i){
        if(i==S.length()-1) return 0;
        if(S.length()%2!=0) return 0;
        int maxValid = 0;
        for(int j=1;j<=S.length();j++){
            if(isValid(S.substring(0,j))==0) maxValid = i;
        }
        return maxValid;

    }
    private char swapChar(char c){
        return c=='('?')':'(';
    }
    private int isValid(String S){
        int bracks = 0;
        if(S.charAt(0) ==')') return -1;
        else bracks++;
        if(S.charAt(S.length()-1) =='(') return -1;
        for (int i=1;i<S.length()-1;i++){
            if(S.charAt(i)=='(') bracks++;
            if(S.charAt(i)==')') bracks--;
            if(bracks<0) return -1;
        }
        return bracks-1;
    }
    public void test(){
      solution("()((()))",2);
    }
}
