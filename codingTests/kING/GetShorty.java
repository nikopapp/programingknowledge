import java.util.Scanner;
import java.util.*;
import java.math.BigDecimal;

public class GetShorty{
  private ArrayList<String> sample = new ArrayList<String>();
  private ArrayList<Case> cases = new ArrayList<Case>();
  public static void main(String[] args){
    GetShorty program = new GetShorty();
    program.run();
  }
  public void run(){
    readFile();
    // printFile();
    separateCases();
    // printCases();
    solveCases();

  }
  private void solveCases(){
    for(Case c:cases)
      solveCase(c);
  }
  private void printCases(){

    for(int i=0;i<cases.size();i++){
      System.out.println("Case "+i);
      for(int j=0;j<cases.get(i).size();j++){
        System.out.println(cases.get(i).get(j));
      }

    }
  }
  private void separateCases(){
    Case c = new Case();
    for(int i=0;i<sample.size();i++){
      if(sample.get(i).split(" ").length==2){
        c = new Case();
        cases.add(c);
      }
      c.add(sample.get(i));
    }

}
  private void readFile(){
    try{
      Scanner sc = new Scanner(System.in);
      while(sc.hasNextLine()){
        sample.add(sc.nextLine());
      }
    }catch (Exception e) {
      System.out.println(e);
    }
  }
  private void solveCase(Case c){
    int n = Integer.parseInt(c.get(0).split(" ")[0]);
    int m = Integer.parseInt(c.get(0).split(" ")[1]);
    System.out.println("n:"+n+", m:"+m);
    ArrayList<Corridor> corridors = new ArrayList<Corridor>();
    for(int i=1;i<c.size();i++){
      String[] tokens = c.get(i).split(" ");
      corridors.add(new Corridor(Integer.parseInt(tokens[0]),
                                  Integer.parseInt(tokens[1]),
                                  Double.parseDouble(tokens[2])
                                  ));
    }
    for(Corridor cor:corridors){
      cor.print();
    }
  }
  private void printFile(){
    for(String s:sample){
      System.out.println(s);
    }
  }

}
class Corridor{
  public int a,b;
  public BigDecimal f;
  public Corridor(int a,int b, Double f){
    this.a = a;
    this.b = b;
    this.f = new BigDecimal(f);
  }
  public void print(){
    System.out.println(a+" "+b+ " "+f);
  }
}


class Case extends ArrayList<String>{
}
