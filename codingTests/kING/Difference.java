import java.io.File;
import java.util.Scanner;
import java.util.*;

public class Difference{
  private File f;
  public static void main(String[] args){
    Difference program = new Difference();
    program.run();
  }
  public void run(){
    readFile();
  }
  private void readFile(){
    try{
      Scanner sc = new Scanner(System.in);
      while (sc.hasNextLong()) {
        long a = sc.nextLong(), b = sc.nextLong();
        System.out.println(difference(a,b));
      }
    }catch (Exception e) {
      System.out.println(e);
    }
  }
  private long difference(long a, long b){
    // if(a<0||b<0) return "Invalid Pair -- Only input positive integers";
    // if(a>1000000000000000L||b>1000000000000000L) return "Invalid Pair -- Only input positive integers lesser or equal to 10^15";
    return a>=b ? (a-b): (b-a);
  }
}
