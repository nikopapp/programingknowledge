
import java.awt.Point;
import java.util.*;
class Geom{
	private ArrayList<Point> points = new ArrayList<Point>();
	private ArrayList<Double> angles = new ArrayList<Double>();
	public Geom(){
		points.add(new Point(0,0));
		points.add(new Point(50,50));
		points.add(new Point(0,100));
		points.add(new Point(-50,50));
		run();
	}
	public static void main(String[] args){
		Geom g = new Geom();
	}
	private void run(){
		printPoints();
		findAngles();
		printAngles();
		Point oldB = points.get(1);
		Point newB = new Point(110,90);		
		points.set(1,newB);
		points.set(2,findC(oldB));
		findAngles();
		printAngles();
		printPoints();


	}
	private void printPoints(){
		System.out.println("Points--------------");	
		for (Point p:points)
			System.out.println(p.x+" "+p.y);
	}
	private void findAngles(){
		for(int i=0;i<points.size();i++){
			Double sX = points.get(i).getX();
			Double eX = points.get((i+1)%points.size()).getX();
			Double sY = points.get(i).getY();
			Double eY = points.get((i+1)%points.size()).getY();
			angles.add((eY-sY)/(eX -sX));
		}
		
	}
	private void printAngles(){
		System.out.println("Angles--------------");
		int count =0;
		for (Double a:angles)
			System.out.println(++count+" "+a);		
	}
	private Point findC(Point oldB){
		Point point = new Point(points.get(2).x+(points.get(1).x-oldB.x),points.get(2).y+(points.get(1).y-oldB.y));
		return point;	
	}
	
}
