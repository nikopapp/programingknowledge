'use strict';

// The wrap module is its own constructor.
wrap();
function wrap() {

window.addEventListener("load", start);
var animation, hasAudio;
var url, slides, slide;
var audio, playing, time, timer, bytes, byte;
var screenWidth = 1024, screenHeight = 768;
var ops = "XYTRGBAVHFEKDCPNSM";
var X=0, Y=1, T=2, R=3, G=4, B=5, A=6, V=7, H=8, F=9;
var E=10, K=11, D=12, C=13, P=14, N=15, S=16, M=17;

// ----- Loading --------------------------------------------------------------

// Prepare everything.
function start() {
    SSO();
    getURL();
    getSlides();
    nameSlides();
    applyTemplates();
    wireUpLinks();
    highlightPrograms();

    checkRecording();
//    addOverlay();
    addPlayer();
    getBookmark();
    if (animation) bytes = encode(animation);
    byte = 0;
    attachHandlers();
//    document.onkeydown = keyPress;
//console.log(slides);
}

// Get rid of a Single Sign On ticket on the URL, if any
function SSO() {
    var here = location.href;
    var ticket = here.indexOf('?ticket=');
    if (ticket < 0) return;
    here = here.substring(0, ticket);
    if (history && history.replaceState) history.replaceState('','',here);
    else location.href = here;
}

// Get the url of the current page.
function getURL() {
    var here = location.href;
    var pos = here.indexOf('?');
    if (pos >= 0) here = here.substring(0, pos);
    pos = here.indexOf('#');
    if (pos >= 0) here = here.substring(0, pos);
    url = here;
}

// Divide the page into slides, according to its top level elements.  The
// slides are classified according to their tag names into templates, sections
// and asides.  The slides are allocated sequential ids, which are also indexes
// into the slides array.  If a slide has an explicit id attribute, it acts as
// a synonym, with an extra keyed entry being added to the array.  A template
// without an id attribute is treated as the default, with synonym 'template'.
function getSlides() {
    var body = document.body;
    var id = 0;
    slides = [];
    for (var i=0; i<body.children.length; i++) {
        var node = body.children[i];
        var synonym = node.id;
        var tag = node.tagName.toLowerCase();
        var slide = { id: id++ };
        if (tag == 'template') slide.type = 'template';
        else if (tag == 'section') slide.type = 'section';
        else if (tag == 'aside') slide.type = 'aside';
        else slide.type = "?";
        if (slide.type != 'template') slide.node = node;
        else {
            if (! synonym) synonym = 'template';
            if (! node.content) slide.node = node;
            else slide.node = document.importNode(node.content, true);
            var here = slide.node.querySelector(".here");
            if (here && here.innerText) {
                slide.startNumber = parseInt(here.innerText);
            }
        }
        slides.push(slide);
        if (synonym) slides[synonym] = slide;
    }
}

// Create a name for each section slide and aside for display. A section slide
// is a numbered main-sequence slide.  An aside is printed, but not displayed,
// other than by following a link, and is named according to the preceding
// section slide, e.g. 3a, 3b,...  Also store prev and next ids for navigation.
// The first slide must be a template, and its 'next' field refers to the title
// slide, i.e. the first section slide.
function nameSlides() {
    var sections = [];
    for (var i=0; i<slides.length; i++) {
        if (slides[i].type == 'section') sections.push(slides[i]);
    }
    var letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var startNo = 0, slideNo = 0, asideNo = 0;
    for (var i=0; i<slides.length; i++) {
        var slide = slides[i];
        if (slide.type == 'template') {
            if (slide.startNumber) startNo = slide.startNumber;
        } else if (slide.type == 'section') {
            slide.name = "" + (startNo + slideNo);
            if (slideNo == 0) slide.prev = null;
            else slide.prev = sections[slideNo-1].id;
            if (slideNo == sections.length - 1) slide.next = null;
            else slide.next = sections[slideNo+1].id;
            slideNo++;
            asideNo = 0;
        } else if (slide.type == 'aside') {
            slide.name = "" + (startNo+slideNo-1) + letters.charAt(asideNo);
            if (slideNo == 0) slide.prev = null;
            else slide.prev = sections[slideNo-1].id;
            if (slideNo == sections.length) slide.next = null;
            else slide.next = sections[slideNo].id;
            asideNo++;
        } else {
            slide.name = '$' + slide.id;
        }
    }
    if (slides[0].type != 'template') throw "No template";
    slides[0].next = sections[0].id;
}

// A template holds items to be copied into each normal slide.  
// Add style and copies of the template objects to each slide.
// The class attribute is used to specify a non-default template.
function applyTemplates() {
    for (var id=0; id<slides.length; id++) {
        var slide = slides[id];
        if (slide.type != 'section' && slide.type != 'aside') continue;
        slide.node.style.position = 'relative';
        slide.node.style.width = screenWidth + "px";
        slide.node.style.height = screenHeight + "px";
        var template = slides['template'];
        var classes = slide.node.classList;
        for (var c=0; c<classes.length; c++) {
            var cls = classes[c];
            if (slides[cls]) template = slides[cls];
        }
        if (template) {
            var templateChildren = template.node.querySelectorAll("div");
            for (var t=0; t<templateChildren.length; t++) {
                var child = templateChildren[t];
                child.style.display = 'block';
                slide.node.appendChild(child.cloneNode(true));
            }
        }
        addNavigation(slide);
    }
}

// Add navigation to a slide.  Replace the contents of .here with the
// name, and make .prev and .next links point to previous and next slides.
function addNavigation(slide) {
    var links = slide.node.getElementsByTagName('a');
    for (var i=0; i<links.length; i++) {
        var link = links[i];
        if (! link.classList) continue;
        if (link.classList.contains('prev')) {
            if (! slide.prev) link.style.visibility = 'hidden';
            else link.href = '#' + slide.prev;
        }
        else if (link.classList.contains('next')) {
            if (! slide.next) link.style.visibility = 'hidden';
            else link.href = '#' + slide.next;
        }
    }
    var here = slide.node.querySelector(".here");
    while (here.firstChild) here.removeChild(here.firstChild);
    here.appendChild(document.createTextNode(slide.name));
}

// Wire up the links which jump between slides.
function wireUpLinks() {
    var links = document.getElementsByTagName("a");
    for (var i=0; i<links.length; i++) {
        var link = links[i];
        if (link.classList.contains("jump")) {
            link.onclick = function(e) { jump(e); }
        }
    }
}

// If hljs is loaded, find all program fragments and highlight them.  Also add
// an optional link to a program file.
function highlightPrograms() {
    if (typeof hljs == undefined) return;
    var languages = [
        "xml", "cpp", "makefile", "haskell", "bash", "javascript", "sql",
        "java", "http", "css"];
    var pres = document.querySelectorAll('pre');
    for (var i=0; i<pres.length; i++) {
        var pre = pres[i];
        var isCode = false;
        for (var n=0; n<languages.length; n++) {
            if (pre.classList.contains(languages[n])) isCode = true;
        }
        if (! isCode) continue;
        hljs.highlightBlock(pre);
        var file = pre.dataset.file;
        if (file) {
            var link = document.createElement('a');
            link.href = file;
            link.style.float = "right";
            link.style.margin = "0";
            link.style.color = "blue";
            link.appendChild(document.createTextNode(file));
            pre.insertBefore(link, pre.firstChild);
        }
        var filename = pre.dataset.filename;
        if (filename) {
            var span = document.createElement('span');
            span.style.float = "right";
            span.style.margin = "0";
            span.style.color = "blue";
            span.appendChild(document.createTextNode(filename));
            pre.insertBefore(span, pre.firstChild);
        }
    }
}

// If there are no global variables 'animation' or 'hasAudio', create
// them and set them to undefined.
function checkRecording() {
    if (typeof window.animation != undefined) animation = window.animation;
    if (typeof window.hasAudio != undefined) hasAudio = window.hasAudio;
}

// Follow a jump link.  Note browser extends "#name" to "url#name".
function jump(e) {
    var id = e.target.href;
    id = id.substring(id.indexOf('#') + 1);
    show(id);
    e.stopPropagation();
    e.preventDefault();
    return false;
}

// Add an audio player component, if there is an audio commentary.  A global
// variable "hasAudio" should be defined in an included script.
function addPlayer() {
    if (! hasAudio) return;
    audio = element("audio", "id", "audio", "controls", "controls");
    audio.style.position = "relative";
    audio.style.bottom = "1.5em";
    audio.style.width = "1024px";
    add(audio, element("source", "type", "audio/mpeg", "src", "index.mp3"));
    add(document.body, audio);
    audio = document.querySelector("#audio");
    time = 0;
}

// Add the canvas overlay, if there's an animation.
function addOverlay() {
    if (! animation) return;
    var canvas = element("canvas", "id", "overlay");
    canvas.width = "1024";
    canvas.height = "768";
    canvas.style.position = "fixed";
    canvas.style.top = "0";
    canvas.style.left = "0";
    canvas.style.pointerEvents = "none";
    add(document.body, canvas);

    var context = canvas.getContext("2d");
    context.strokeStyle = '#f0f';
    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(1023, 0);
    context.lineTo(1023, 767);
    context.lineTo(0, 767);
    context.lineTo(0, 0);
    context.stroke();
}

// Attach handlers for scaling, key presses, the play button, and audio events.
function  attachHandlers()
{
    rescale();
    window.onresize = rescale;
    document.onkeydown = keyPress;
    if (! audio) return;
    audio.addEventListener("play", play);
    audio.addEventListener("pause", pause);
    audio.addEventListener("timeupdate", slider);
/*
    var playButton = document.getElementById("play");
    if (playButton) playButton.onclick = audioStart;
    audio.addEventListener("timeupdate", audioTick);
    audio.addEventListener("ended", audioEnd);
*/
}

function slider() {
    var audioTicks = Math.floor(audio.currentTime * 100);
    console.log("slide", audioTicks);
}

// Create an element, optionally with given attributes.
function element(tag, name1, value1, name2, value2)
{
    var item = document.createElement(tag);
    if (! name1) return item;
    item.setAttribute(name1, value1);
    if (! name2) return item;
    item.setAttribute(name2, value2);
    return item;
}

// Create a text node
function text(s)
{
    return document.createTextNode(s);
}

// Add an element as a child to another element
function add(element, child)
{
    element.appendChild(child);
}

// ----- Display --------------------------------------------------------------

// In normal mode, one slide is displayed, stretched to fit the window,
// regardless of aspect ratio.  This works well for mobiles.  To view with the
// right aspect ratio on a desktop, the Window Resizer plugin can be installed.
/*
// Arrange for scaling.
function startDisplay() {
    rescale();
    window.onresize = rescale;
}
*/
// Scale everything at the start, or when the window is resized.
function rescale() {
    var html = document.documentElement;
    var body = document.body;
    body.width = screenWidth + "px";
    body.height = screenHeight + "px";
    body.style.overflow = 'hidden';
    var windowHeight = html.clientHeight;
    var windowWidth = html.clientWidth;
    var scaleX = windowWidth / screenWidth;
    var scaleY = windowHeight / screenHeight;
    body.style.transformOrigin = '0 0';
    body.style.transform = ' scale(' + scaleX + ',' + scaleY + ')';
}

// ----- BookMarking ----------------------------------------------------------

// Remember the current section id using localStorage.  Only do it for local
// users within https, so we don't have to ask permission.
function setBookmark() {
    if (url.lastIndexOf("https", 0) != 0) return;
    localStorage.setItem(url+"#slide", slide.id);
}

// Get the remembered slide or aside.  Allow #id on the url to override.
function getBookmark() {
    var here = location.href;
    var pos = here.indexOf('?');
    if (pos >= 0) here = here.substring(0, pos);
    pos = here.indexOf('#');
    if (pos >= 0) {
        here = here.substring(pos+1);
        if (here in slides) { show(here); return; }
    }
    var id = localStorage.getItem(url+"#slide");
    if (id) show(id);
    else show(slides[0].next);
}

// ----- User Interaction -----------------------------------------------------

// Deal with key presses.
function keyPress(event) {
    if (!event) event = window.event;
    var key = event.keyCode;
    var enterKey = 13, spaceBar = 32, backSpace = 8;
    var cKey = 67, nKey = 78, pKey = 80, tKey = 84;
    var pageUp = 33, pageDown = 34, homeKey = 36, endKey = 35;
    var leftArrow = 37, upArrow = 38, rightArrow = 39, downArrow = 40;
    if (key == pageDown || key == rightArrow || key == downArrow ||
        key == spaceBar || key == enterKey) {
        if (animation && ! playing) play();
        else skipForward();
    }
    else if (key == pageUp || key == leftArrow || key == upArrow ||
             key == backSpace) {
        if (playing) pause();
        else skipBackward();
    }
    else if (key == pKey) preview();
}

// Prepare for printing by making all slides visible
function preview() {
    var style = "position: relative; display: block; width: 1024px;" +
        "height: 768px; minHeight: 0;";
    for (var i=0; i<slides.length; i++) {
        var section = slides[i];
        if (section.type == 'section' || section.type == 'aside') {
            section.node.style.cssText = style;
        }
    }
}

// Skip forward when playing.
function skipForward() {
    if (slide.next) show(slide.next);
}

// Skip backward when paused.
function skipBackward() {
    if (slide.prev) show(slide.prev);
}

// Display a slide or aside
function show(id) {
    if (slide) {
        slide.node.style.display = 'none';
    }
    slide = slides[id];
    if (! slide) slide = slides[slides[0].next];
    slide.node.style.display = 'block';
    setBookmark(slide.id);
}

// Start the animation timer and the audio.
function play() {
    if (playing) return;
    playing = true;
    timer = setInterval(tick, 10);
    if (audio && audio.paused) audio.play();
}

// Stop the timer and audio
function pause() {
    if (! playing) return;
    playing = false;
    clearInterval(timer);
    if (audio && ! audio.paused) audio.pause();
}

// This is called regularly while the audio is playing.  It brings the
// animation time up to date with the audio time.  If the animation finishes
// first, it continues to increment animation time.  If the audio finishes
// first, the animation stops.  Note that animation time can get ahead of audio
// time, but by less than 0.64 seconds.
function tick() {
    var audioTicks = Math.floor(audio.currentTime * 100);
    while (time < audioTicks) step();
    if (audio.currentTime === audio.duration) pause();
}

// ----- Animation ------------------------------------------------------------

// Do one step of the animation
function step() {
    if (byte >= bytes.length) { time++; return; }
    var op, arg;
    decode();
    switch (op) {
    case N: show(slide.next); break;
    case T: time += arg; break;
    default: time++; console.log("?", op); break;
    }

    // Decode one op and arg from the bytecode array.
    function decode()
    {
        var b = bytes[byte++];
        op = b & 0x03;
        arg = b >> 2;
        if (op == 2) arg = arg & 0x3f;
        if (op != 3) return;
        op = ((arg >> 1) & 0x1f) + 3;
        if (arg & 1) arg = bytes[++b];
        else arg = 0;
    }
}

/* Compress a textual animation into an array of bytes.  In the text, each op
is an upper case letter, optionally followed by a numerical argument, which is
decimal, or # followed by lower case hex, or a character in single quotes.  In
the compressed bytecode, the least two significant bits of a byte form the
basic opcode, one of X, Y, T, OP.  For X and Y, the remaining 6 bits form a
signed operand, for T they form an unsigned operand.  For OP the next
significant bit indicates whether there is an argument in the following byte,
or whether the argument is assumed to be zero, and the remaining bits form an
extended opcode.  Opcodes are:

  X dx    change in x position
  Y dy    change in y position
  T dt    change in time, unsigned, in 1/100ths of a second
  R n     change red component of colour
  G n     change green component of colour
  B n     change blue component of colour
  A n     change opacity of colour
  V i     make element with id "i" visible
  H i     hide element with id "i"
  F f     set duration of frame as percentage of normal
  E i     set text focus in element "i"
  K k     simulate typing a key k
  D d     simulate a directional arrow key (0=<, 1=^, 2=>, 3=v)
  C 0     clear canvas
  P 0     previous slide
  N 0     next slide
  S 0     stop and wait for manual continuation
  M 0     mark a pause point
*/

function encode(text) {
    var bytes = null;
    var t = 0, b = 0;

    while (t < text.length) code();
    bytes = new Int8Array(b);
    t = b = 0;
    while (t < text.length) code();
    return bytes;

    // Encode one instruction
    function code() {
        var op = opcode();
        var arg = argument();
        if (op == T && arg < 0) throw("Negative argument");
        if (op > T && arg > 255) throw("Arg out of range");
        if (op > T && arg < -128) throw("Arg out of range");
        if (op == X || op == Y) {
            while (arg > 31) { write((31<<2) + op); arg = arg - 31; }
            while (arg < -32) { write(((-32)<<2) + op); arg = arg + 32; }
            write((arg<<2) + op);
        }
        else if (op == T) {
            while (arg > 63) { write((63<<2) + op); arg = arg - 63; }
            write((arg<<2) + op);
        }
        else if (arg == 0) {
            write(((op-3)<<3) + 3);
        }
        else {
            write(((op-3)<<3) + 7);
            write(arg);
        }
    }

    // Get opcode
    function opcode() {
        var ch = text[t++];
        var op = -1;
        for (var i=0; i<ops.length; i++) if (ch == ops[i]) op = i;
        if (op < 0) throw("Bad op " + ch + " at position " + (t-1));
        return op;
    }

    // Get argument
    function argument() {
        while (text[t] == ' ') t++;
        var ch = text[t];
        var arg = 0;
        if ('0' <= ch && ch <= '9' || ch == '-' || ch == '+') arg = integer();
        else if (ch == '#') arg = hex();
        else if (ch == "'") arg = char();
        return arg;
    }

    // Read an integer from the text
    function integer()
    {
        var n = 0, neg = false;
        if (text[t] == '+') t++;
        else if (text[t] == '-') { t++; neg = true; }
        while (isDigit(text[t])) { n = n*10 + (text[t] - '0'); t++; }
        if (neg) n = -n;
        return n;
    }

    // Read a hex number from the text
    function hex()
    {
        var n = 0;
        t++;
        while (true) {
            var ch = text[t];
            if (isDigit(ch)) { n = n*16 + (ch - '0'); t++; }
            if ('a' <= ch && ch <= 'f') { n = n*16 + (ch - 'a'); t++; }
            else break;
        }
        return n;
    }

    // Read a quoted character from the text
    function char()
    {
        t++;
        var c = text[t++];
        t++;
        return c.charCodeAt(0);
    }

    // Convert a character '0'..'9','a'..'f','A'..'F' into a number.
    function digit(b)
    {
        if (b >= 'A' && b <= 'F') return b - 'A';
        if (b >= 97 && b <= 102) return 10 + b - 97;
        if (b >= 65 && b <= 70) return 10 + b - 65;
    }

    // Check whether a character is a digit
    function isDigit(ch)
    {
        if (ch >= '0' && ch <= '9') return true;
        return false;
    }

    // Write a byte to the bytecode array, or just count the byte.
    function write(byte) {
        if (bytes) bytes[b++] = byte;
        else b++;
    }
}

function decode(bytes) {
    var s = "";
    for (var b=0; b<bytes.length; b++) {
        var byte = bytes[b];
        var op = byte & 3;
        var arg = byte >> 2;
        if (op == 2) arg = arg & 0x3f;
        else if (op == 3) {
            op = ((arg >> 1) & 0x1f) + 3;
            if (arg & 1) arg = bytes[++b];
            else arg = 0;
        }
        s += ops[op];
        if (arg != 0) s += arg;
    }
    return s;
}

// Check if a string starts with a prefix, using the efficient trick.
function startsWith(s, prefix) {
    return s.lastIndexOf(prefix, 0) == 0;
}

// End of wrap module
}

// A marker is a position within a slideshow's animation.  It is a point at
// which the canvas is clear, including the start of every slide.  Markers help
// when the visitor goes backwards in time.
function Marker(b,t,s,c) {
    var marker = {byte: b, time: t, slide: s, clear: c};
}

// Constructor for slides

