<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB" xml:lang="en-GB">
<!-- Deliver as XHTML -->
<head>
<meta charset="UTF-8"/>
<meta name="author" content="ian"/>
<meta id="bristol" name="keywords" content="dummy"/>
<meta id="mybristol" name="keywords" content="dummy"/>
<meta id="contacts" name="keywords" content="dummy"/>
<meta id="gmailstaff" name="keywords" content="dummy"/>
<meta id="gmailstudents" name="keywords" content="dummy"/>
<title>OOP with Java</title>
<link href="../share/style2.css" rel="stylesheet" type="text/css" />
<link href="/wrap/hljs/styles/xcode.css" rel="stylesheet" type="text/css" />
<script src='/wrap/hljs/highlight.pack.js'></script>
<script src='../share/wrap.js'></script>
</head>
<body>

<template>
  <div class='icon'>
    <img alt="recycling" src="recycle.png" />
  </div>
  <div class='navigation'>
    <a class="jump prev" href="">&lt; </a>
    <span class="here">1</span>
    <a class="jump next" href=""> &gt;</a>
  </div>
</template>

<section>
<h1 class="title">Garbage Collection</h1>
</section>

<section>
<h2>RTS</h2>
<p>The idea in this chapter is to look beneath the surface at what happens in
the Java <dfn>Run Time System</dfn> (RTS)</p>
<p>One of the main tasks is to look after memory management automatically, and
that involves memory allocation and <dfn>Garbage Collection</dfn></p>
<p>These days, it ought to be renamed as Recycling</p>
</section>

<section>
<h2>Heap</h2>
<p>There are some things such as string constants which Java may be able to
allocate permanently at the start of execution (static allocation)</p>
<p>But we'll ignore that - what's interesting is dynamic allocation of
memory</p>
<p>Dynamic allocation is done in a <dfn>heap</dfn>, which is a managed area of
memory</p>
</section>

<section>
<h2>Objects</h2>
<p>Memory is allocated from the heap in lumps called <dfn>objects</dfn>, which
means almost exactly the same thing at the run time level as at the language
level</p>
<p>What's important about an object from the point of view of the RTS is its
size and shape</p>
</section>

<section>
<h2>Size and shape</h2>
<p>The <dfn>size</dfn> of an object is the amount of memory it uses</p>
<p>The <dfn>shape</dfn> of an object is the set of locations of its
non-primitive fields, i.e. the fields which are references or pointers to other
objects</p>
<p>Technically, a <dfn>reference</dfn> need not be a pointer, but we'll ignore
that</p>
<p>The size and shape are defined by the object's class, which is defined by a
static or dynamic pointer or index in the object's header, and may or may not
be included in the shape pointers</p>
</section>

<section>
<h2>Roots</h2>
<p>A <dfn>root</dfn> is a pointer to an object which is actively being used at
a particular moment</p>
<p>Roots are typically taken to be in:</p>
<ul>
<li>processor registers (in use by the current thread)</li>
<li>dumped processor registers (suspended threads)</li>
<li>processor stacks (in use by function/method calls which are in
progress)</li>
<li>any objects you want to keep permanent</li>
</ul>
</section>

<section>
<h2>Reachability</h2>
<p>An object is <dfn>reachable</dfn> if you can get to it by following pointers
from any of the roots</p>
<img class="middle" src="reachable.png"/>
</section>

<section>
<h2>Features</h2>
<p>There can be, and often are, pointers from unreachable objects to reachable
ones</p>
<p>An object which has no pointers pointing to it must be unreachable</p>
<p>Pointers between objects can form <dfn>cycles</dfn></p>
<p>Because of cycles, there can be a group of unreachable objects where every
object has at least one pointer to it</p>
</section>

<section>
<h2>Reference counting</h2>
<p>Suppose each object contains a number, called a <dfn>reference count</dfn>,
which keeps track of how many pointers there are pointing to the object</p>
<p>And suppose that, when the reference count of an object falls to zero, it is
somehow marked as garbage</p>
<p>This is the basis of one kind of garbage collection</p>
</section>

<section>
<h2>Barriers</h2>
<p>With any garbage collection scheme, a <dfn>barrier</dfn> is a piece of code
inserted by the compiler during normal execution which makes a quick test</p>
<p>The test usually fails, but when it succeeds, a supporting function is
called</p>
<p>In the reference counting scheme, the barrier code might be:</p>
<pre class="C">if (--object->count == 0) {
  reclaim(object);
}
</pre>
</section>

<section>
<h2>Disadvantages</h2>
<p>One problem is that reference counting garbage collection is unable to
detect cycles of garbage objects</p>
<p>Another problem is the potential inefficiency of keeping track of reference
counts of objects during execution</p>
<p>Another problem is the difficulty of dealing efficiently with thread
synchronisation on multiple or multi-core processors</p>
<p>As a result, reference counting garbage collection was unpopular for a long
time</p>
</section>

<section>
<h2>Advantage</h2>
<p>One huge advantage of reference counting garbage collection is that the
detection of garbage happens concurrently with execution</p>
<p>That potentially allows for garbage collection to be done without any
(noticeable) pauses of execution</p>
<p>Modern hybrid garbage collectors involve barrier code, so reference counting
barrier code isn't so expensive</p>
<p>When reference counting can be made to work properly, it can be extremely
effective - it is making a comeback</p>
</section>

<section>
<h2>Mark and sweep</h2>
<p>Many garbage collector algorithms are based on some variation of
the <dfn>mark and sweep</dfn> idea</p>
<p>They are easiest to explain as <dfn>stop-the-world</dfn> algorithms</p>
<p>That means execution is paused every so often, a round of garbage collection
is done, then execution continues</p>
<p>A lot of work is done to try to make various phases of the algorithms act
concurrently with execution, to avoid long pauses</p>
</section>

<section>
<h2>Fixed size objects</h2>
<p>The original mark and sweep algorithm was based on the fact that all objects
were of the same size</p>
<p>There was room in an object for a couple of bits indicating the type, and
two pointers</p>
<p>Alternatively, there could be one pointer and some primitive data, or
all primitive data</p>
<p>Larger 'objects' were built from chains of smaller ones</p>
</section>

<section>
<h2>The marking phase</h2>
<p>Each object has one bit associated with it, called a <dfn>mark bit</dfn>,
either inside it, or in a separate array of bits</p>
<p>The bit is set if the object has been visited during the marking phase of
the algorithm</p>
<p>During the phase, each reachable object is first <dfn>marked</dfn> and
then later <dfn>scanned</dfn></p>
</section>

<section>
<h2>The marking queue</h2>
<p>A queue (or stack) is maintained of objects which have been marked, but not
yet scanned</p>
<p>The queue is initialised by finding all the root pointers, marking the
objects they point to, and entering them into the queue</p>
<p>In a loop, one object is taken off the queue and <dfn>scanned</dfn>,
i.e. its shape information is used to find the pointers within in, and for each
pointer, if it points to an unmarked object, the object is marked and put on
the queue</p>
</section>

<section>
<h2>The sweep phase</h2>
<p>When the marking queue becomes empty, all the reachable objects have been
marked</p>
<p>What's left is to find the unmarked objects, which are the garbage objects,
and to arrange to re-use their memory space</p>
<p>Sweeping consists of a loop which runs through all the objects, looking for
unmarked ones and putting them on a chain of object-sized gaps (and
unmarking the marked ones)</p>
</section>

<section>
<h2>Chain allocation</h2>
<p>When all the objects are the same size, there is never any need to move the
objects</p>
<p>The free gaps can be stored in a <dfn>chain</dfn> (i.e. each contains a
pointer to the next)</p>
<p>To allocate space for a new object, one gap is removed from the front of the
chain</p>
</section>

<section>
<h2>Block allocation</h2>
<p>Another approach which can be taken, used in modern versions of memory
management in C with <code>alloc</code> and <code>free</code> replacements, or
with automatic memory management libraries, is to allocate objects in
blocks</p>
<p>The idea is that, to avoid moving objects during garbage collection, objects
are allocated in separate areas, with each area containing objects of the same
size</p>
<p>There is then one chain of gaps for each possible size</p>
<p>Sizes are often rounded up, to reduce the number of different sizes</p>
</section>

<section>
<h2>Fragmentation</h2>
<p>Whenever there are areas of memory which are unusable indefinitely, that's
called <dfn>fragmentation</dfn></p>
<p>Any scheme which has fixed-size objects, or where sizes are rounded up,
has internal fragmentation - wasted space inside the objects</p>
<p>Any scheme which has variable sized objects, but which avoids moving the
objects, suffers from external fragmentation - wasted gaps between objects</p>
<p>To minimize fragmentation, you need variable-sized objects which are
moved during garbage collection</p>
</section>

<section>
<h2>Copying collection</h2>
<p>Are there algorithms which move objects during garbage collection, so that
there are no long-lived gaps?</p>
<p>There are, and <dfn>copying</dfn> is the simplest scheme</p>
<p>This starts with two equal-sized areas, which are used alternately</p>
<p>At garbage collection time, one holds the current heap, and the other is
empty</p>
</section>

<section>
<h2>The copying algorithm</h2>
<p>The algorithm is a variant of mark and sweep</p>
<p>As each object in the old heap is visited, it is copied into the new heap,
and replaced by a marked indirection object containing a <dfn>forwarding</dfn>
pointer</p>
<p>There is a queue pointer between the start and leading edge of the new heap,
with objects to the right representing the queue of marked but not scanned
objects</p>
<p>When the queue pointer reaches the leading edge, copying is complete</p>
</section>

<section>
<h2>The copying queue</h2>
<img class="middle" src="copying.png"/>
<p>The <code>next</code> pointer is where newly copied objects are pushed -
the <code>queue</code> pointer is the next object to scan</p>
<p>Using forwarding, the internal pointers are updated to point to the new heap
rather than the old one</p>
</section>

<section>
<h2>Pros and cons</h2>
<p>In favour of the copying algorithm is that (a) it allows simple <dfn>pointer
bumping</dfn> allocation (b) it is quite quick (c) the effort is proportional
to the number of reachable objects - garbage objects are not touched (d) the
new heap has better locality of reference</p>
<p>The main disadvantage is that only half of the available memory is in use at
once - as a result is is normally only used as a component of a hybrid
system</p>
</section>

<section>
<h2>Compacting collection</h2>
<p>The extra empty space can be avoided using a <dfn>compacting</dfn>
collector</p>
<p>During marking, for each object, a chain is built up of pointers which point
to that object</p>
<p>Then there is a loop which runs through the heap and works out the target
position of each object after compacting (with overlaps)</p>
<p>Then the chains are scanned, so that all the pointers from objects are
updated to point to the new locations</p>
<p>Then the objects are actually moved, leaving no gaps</p>
</section>

<section>
<h2>Generational collection</h2>
<p>With <dfn>generational</dfn> collection, there is a relatively small young
object space, dealt with using (fast) copying, and an old object space, dealt
with using a (slow) compacting algorithm</p>
<p>This works well when most garbage is young, i.e. a lot of objects are
short-lived</p>
</section>

<section>
<h2>Garbage first collection</h2>
<p>With <dfn>garbage first</dfn> collection, which is another hybrid idea, the
heap is divided into equal-sized areas</p>
<p>At each garbage collection, the area containing the most garbage is
chosen</p>
<p>The chosen area is moved to an empty area using a copying technique</p>
</section>

<section>
<h2>Ideal collection</h2>
<p>The fact that there are lots of garbage collection algorithms in use, and
that there is a <em>huge</em> literature on the subject, is a sure sign that it
is an unsolved problem</p>
<p>It is interesting to work out what ideal garbage collection would be like,
and whether it is achievable</p>
</section>

<section>
<h2>Ideal features</h2>
<p>An ideal garbage collector should:</p>
<ul>
<li>be accurate, i.e. guarantee to find all garbage</li>
<li>move objects, to avoid fragmentation</li>
<li>avoid long pauses of execution</li>
<li>guarantee a small percentage overhead of time</li>
<li>support multiple threads and processors</li>
</ul>
</section>

<section>
<h2>Nearly ideal systems</h2>
<p>Two systems that come close to the ideal are Azul's massively-parallel
hardware-assisted system:</p>
<p>
<a href="http://www.azulsystems.com/products/whitepaper/wp_pgc.pdf">www.azulsystems.com/products/whitepaper/wp_pgc.pdf</a><br/>
<br/>
and IBM's reference-counting-based Recycler which shows how reference
counting can be made massively parallel yet fully concurrent, but which would
need hardware assistance to move objects efficiently<br/>
<a style="font-size:smaller"
href="http://researcher.watson.ibm.com/researcher/view_group.php?id=3385">researcher.watson.ibm.com/researcher/view_group.php?id=3385</a>
</p>
</section>

<section>
<h2>Conclusion</h2>
<p>The conclusion is that:</p>
<p>Ideal garbage collection with guarantees is impossible without hardware
support, though some systems are 'good' in practice</p>
<p>Today's hardware doesn't have that support</p>
<p>Tomorrow's hardware could have that support, once we've worked out what it
should be</p>
</section>

</body>
</html>
