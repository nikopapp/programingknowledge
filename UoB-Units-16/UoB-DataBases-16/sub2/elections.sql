DROP TABLE IF EXISTS Rawdata;
DROP TABLE IF EXISTS Candidate;
DROP TABLE IF EXISTS Party;
DROP TABLE IF EXISTS Ward;

CREATE TABLE Rawdata (
  date TEXT, ward TEXT,
  electorate TEXT, candidate TEXT,
  party TEXT, votes INT,
  percent TEXT);

.mode csv
.import elections.csv Rawdata
.mode column
.header on
.explain on
.width 20 15 15 10 10 10 10
.nullvalue ***
SELECT COUNT(1) FROM Rawdata;

PRAGMA foreign_keys = 1;

CREATE TABLE Party (
  id INTEGER PRIMARY KEY NOT NULL,
  name STRING UNIQUE NOT NULL
);

CREATE TABLE Ward (
  id INTEGER PRIMARY KEY NOT NULL,
  name STRING NOT NULL,
  electorate INT NOT NULL
);

CREATE TABLE Candidate (
  id INTEGER PRIMARY KEY,
  name STRING UNIQUE NOT NULL,
  party INT,
  ward INT,
  votes INT,
  FOREIGN KEY(party) REFERENCES Party(id),
  FOREIGN KEY(ward) REFERENCES Ward(id)
);

-- Party
INSERT INTO Party(name) SELECT DISTINCT (party) FrOM Rawdata;

-- -- Ward

INSERT INTO Ward (name,electorate)
SELECT  DISTINCT ward,CAST (electorate AS INTEGER) FROM Rawdata;


-- -- Candidates

INSERT INTO Candidate (name,ward,party,votes)
SELECT candidate, Ward.id,Party.id,votes FROM
Rawdata INNER JOIN Party ON Rawdata.party=Party.name
INNER JOIN Ward on Rawdata.ward=Ward.name
GROUP BY candidate;
