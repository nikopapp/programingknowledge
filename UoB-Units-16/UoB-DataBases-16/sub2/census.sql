DROP TABLE IF EXISTS Statistic;
DROP TABLE IF EXISTS Occupation;
DROP TABLE IF EXISTS Ward;
DROP TABLE IF EXISTS County;
DROP TABLE IF EXISTS Region;
DROP TABLE IF EXISTS Country;
.mode csv
.read census/setup.sql
.import census/Country.csv Country
.import census/Region.csv Region
.import census/County.csv County
.import census/Ward.csv Ward
.import census/Occupation.csv Occupation
.import census/Statistic.csv Statistic

.echo ON
.nullvalue NULL
.mode column
.header ON
.width 10 10 10 10 10 10 10 10 10 10 10
.output cesnus_np15685.txt

-- Query 1 ---------------
.print ''
.print 'Query 1'
SELECT Ward.name,
Ward.code
AS c1,
County.name
AS county,
County.code
AS c2,
Region.name
AS region,
Region.code
AS c3,
Country.name
AS country,
Country.code
AS c1 FROM
Ward LEFT JOIN County
ON Ward.parent=County.code
LEFT JOIN Region
ON County.parent=Region.code
LEFT JOIN Country
ON Region.parent=Country.code
Where Ward.code = 'W05000078'
OR County.code = '?'
OR Region.code = '?'
OR Country.code = '?';
-- Query 2 ---------------
.print ''
.print 'Query 2'
SELECT data
AS 'Women working on sales sector' from Statistic
Where wardID='E05001979'
AND gender = 1
AND occId = 7;


-- Query 3 ---------------
.print ''
.print 'Query 3'
SELECT SUM(data)
AS 'admin secret workers'
FROM Statistic
WHERE wardId ='E05000697'
AND occId='4';


-- Query 4 ---------------
.print ''
.print 'Query 4'
SELECT occupation,
SUM(num)
AS workers
FROM(
  SELECT occId,name AS occupation,
  data AS num FROM
  Statistic LEFT JOIN Occupation
  ON Statistic.occId=Occupation.id
  WHERE wardId ='E05003701'
)
GROUP BY occId;


-- Query 5 ---------------
.print ''
.print 'Query 5'
SELECT tot,wardId AS code ,
Ward.name AS ward ,
County.name AS county
FROM ((
  SELECT SUM(data) AS tot, wardId,gender FROM Statistic
  GROUP BY wardId
  ORDER BY tot DESC
  LIMIT 1
)
LEFT JOIN Ward
ON wardID=Ward.code
LEFT JOIN County
ON Ward.parent=County.code
) UNION

SELECT tot,wardId AS code ,
Ward.name AS ward ,
County.name AS county
FROM (
  SELECT SUM(data) AS tot, wardId,gender FROM Statistic
  GROUP BY wardId
  ORDER BY tot ASC
  LIMIT 1
)
LEFT JOIN Ward
ON wardID=Ward.code
LEFT JOIN County
ON Ward.parent=County.code;


-- Query 6 ---------------
.print ''
.print 'Query 6'
SELECT COUNT(1) AS "wards with > 10000 workers"
 FROM (
  SELECT SUM(data) AS tot ,wardId FROM Statistic
  GROUP BY wardId
)
WHERE tot>10000;


--Query 7 ----------------
.print ''
.print 'Query 7'

SELECT "All dataset" AS name, 1.0*SUM(totalworkers)/COUNT(1) AS average FROM(
  SELECT SUM(data) AS totalworkers FROM Statistic
  GROUP BY wardId
)
UNION
SELECT "England" AS name, 1.0*SUM(totalworkers)/COUNT(1) AS average FROM(
  SELECT SUM(data) AS totalworkers FROM Statistic
  WHERE wardId LIKE 'E%'
  GROUP BY wardId
)
UNION
SELECT Region.name AS "name",AVG(tot) AS "average"
FROM (
  SELECT SUM(data) AS tot ,wardId FROM Statistic
  WHERE wardId LIKE 'E%'
  GROUP BY wardId
)
LEFT JOIN Ward
ON wardId=Ward.code
LEFT JOIN County
ON Ward.parent=County.code
LEFT JOIN Region
ON County.parent=Region.code
GROUP BY Region.name
;

--Query 8 ----------------
.print ''
.print 'Query 8'
SELECT Region.name,SUM(totM) AS men ,SUM(totF) AS women , 1.0*SUM(totF)/SUM(totM) AS "women to men ratio" FROM (

SELECT SUM(data) AS totM ,wardId AS keyM , gender FROM Statistic M
WHERE gender =0 AND occId=1 AND wardId LIKE 'E%'
GROUP BY wardId
) LEFT JOIN (

SELECT SUM(data) AS totF ,wardId AS keyF, gender FROM Statistic F
WHERE gender =1 AND occId=1
GROUP BY wardId
) ON keyM=keyF
LEFT JOIN Ward
ON keyM=Ward.code
LEFT JOIN County
ON Ward.parent=County.code
LEFT JOIN Region
ON County.parent=Region.code
GROUP BY Region.name
ORDER BY "women to men ratio" ASC
;

--Query 9 ----------------
.print ''
.print 'Query 9'
SELECT cname AS name,
ROUND(100.0*perOccId1/totalperWard ,1) AS c1 ,
ROUND(100.0*perOccId2/totalperWard ,1) AS c2 ,
ROUND(100.0*perOccId3/totalperWard ,1) AS c3 ,
ROUND(100.0*perOccId4/totalperWard ,1) AS c4 ,
ROUND(100.0*perOccId5/totalperWard ,1) AS c5 ,
ROUND(100.0*perOccId6/totalperWard ,1) AS c6 ,
ROUND(100.0*perOccId7/totalperWard ,1) AS c7 ,
ROUND(100.0*perOccId8/totalperWard ,1) AS c8 ,
ROUND(100.0*perOccId9/totalperWard ,1) AS c9
FROM (
  SELECT C1.code AS key,
  C1.name AS cname,
  C1.parent
  FROM County C1
  INNER JOIN Region
  ON C1.parent=Region.code
  WHERE Region.name = "South East"
) INNER JOIN (
    SELECT County.code AS key1,
    SUM(data) AS perOccId1  FROM Statistic
    LEFT JOIN Ward ON wardId=Ward.code
    LEFT JOIN County ON Ward.parent=key1
    WHERE occId=1
    GROUP BY key1,occid
   )
ON key=key1
-- --------------
INNER JOIN (
  SELECT County.code AS key2,SUM(data) AS perOccId2  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key2
     WHERE occId=2
     GROUP BY key2,occid
)
ON key=key2
-- --------------
INNER JOIN (
  SELECT County.code AS key3,SUM(data) AS perOccId3  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key3
     WHERE occId=3
     GROUP BY key3,occid
)
ON key=key3
-- --------------
INNER JOIN (
  SELECT County.code AS key4,SUM(data) AS perOccId4  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key4
     WHERE occId=4
     GROUP BY key4,occid
)
ON key=key4
-- --------------
INNER JOIN (
  SELECT County.code AS key5,SUM(data) AS perOccId5  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key5
     WHERE occId=5
     GROUP BY key5,occid
)
ON key=key5
-- --------------
INNER JOIN (
  SELECT County.code AS key6,SUM(data) AS perOccId6  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key6
     WHERE occId=6
     GROUP BY key6,occid
)
ON key=key6
-- --------------
INNER JOIN (
  SELECT County.code AS key7,SUM(data) AS perOccId7  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key7
     WHERE occId=7
     GROUP BY key7,occid
)
ON key=key7
-- --------------
INNER JOIN (
  SELECT County.code AS key8,SUM(data) AS perOccId8  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key8
     WHERE occId=8
     GROUP BY key8,occid
)
ON key=key8
-- --------------
INNER JOIN (
  SELECT County.code AS key9,SUM(data) AS perOccId9  FROM Statistic
  LEFT JOIN Ward ON wardId=Ward.code
     LEFT JOIN County ON Ward.parent=key9
     WHERE occId=9
     GROUP BY key9,occid
)
ON key=key9 NATURAL JOIN (
SELECT cname, SUM("per occupation") AS totalperWard FROM (
SELECT County.name AS cname, County.code AS key2,SUM(data) AS "per occupation"
FROM Statistic LEFT JOIN Ward ON wardId=Ward.code
   LEFT JOIN County ON Ward.parent=key2
   LEFT JOIN Region ON County.parent=Region.code
   WHERE Region.name="South East"
   GROUP BY key2,occid
 )
 GROUP BY key2)
 ORDER BY name;

 --Query 10 ----------------
.print ''
.print 'Query 10'
SELECT * FROM (
  SELECT County.name AS CLU, Occupation.name AS occupation
   ,CASE gender
  WHEN 1 THEN 'F'
  WHEN 0 THEN 'M'
  ELSE 'X' END AS gender,
  SUM(data) AS count FROM
  Statistic LEFT JOIN Ward
  ON wardId=Ward.code
  Left JOIN County
  ON Ward.parent=County.code
  LEFT JOIN Region
  ON County.parent=Region.code
  LEFT JOIN Occupation
  ON occId=Occupation.id
  WHERE Region.name="North East"
  GROUP BY County.name,occId,gender
)
WHERE count > 10000
ORDER BY count ASC
;

.output stdout
.echo OFF
