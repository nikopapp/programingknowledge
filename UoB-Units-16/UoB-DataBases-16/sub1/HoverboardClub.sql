.open hoverboard.db;
DROP TABLE IF EXISTS Member;
DROP TABLE IF EXISTS Comittee;
DROP TABLE IF EXISTS Event;
DROP TABLE IF EXISTS Attends;
DROP TABLE IF EXISTS Skill;
DROP TABLE IF EXISTS memberSkills;
.header on
.mode column
PRAGMA foreign_keys = 1;
CREATE TABLE Member(
  memberID INT PRIMARY KEY NOT NULL,
  stuID STRING,
  name STRING NOT NULL,
  email STRING UNIQUE NOT NULL,
  ridingLevel INT NOT NULL,
  CHECK(ridingLevel>=0)
);
CREATE TABLE Skill(
  skillID INT PRIMARY KEY NOT NULL,
  name STRING UNIQUE NOT NULL
);
CREATE TABLE memberSkills(
  skillID INT NOT NULL,
  memberID INT NOT NULL,
  level INT NOT NULL,
  CHECK(level>0),
  CHECK(level<=10),
  FOREIGN KEY (memberID) REFERENCES Member(memberID),
  FOREIGN KEY (skillID) REFERENCES Skill(skillID),
  PRIMARY KEY (memberID,skillID)
);
CREATE TABLE Comittee(
  comitteeID INT PRIMARY KEY NOT NULL,
  role STRING UNIQUE NOT NULL,
  holder STRING UNIQUE NOT NULL,
  FOREIGN KEY (holder) REFERENCES Member(memberID)
);
CREATE TABLE Event(
  eventID INT PRIMARY KEY NOT NULL,
  name STRING NOT NULL,
  location STRING NOT NULL,
  event_date DATE ,
  description STRING,
  organiser INT  NOT NULL,
  CHECK(event_date>0),
  FOREIGN KEY (organiser) REFERENCES Member(memberID),
  UNIQUE(event_date,location)
);
CREATE TABLE Attends(
  eventID INT NOT NULL,
  memberID STRING NOT NULL,
  FOREIGN KEY (memberID) REFERENCES Member(memberID),
  FOREIGN KEY (eventID) REFERENCES Event(eventID),
  PRIMARY KEY (memberID,eventID)
);
.schema
