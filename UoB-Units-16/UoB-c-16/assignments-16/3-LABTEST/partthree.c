#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define I2CHAR 65
#define A_SIZE 10
#define ITER   10

void fillArray(char A[A_SIZE][A_SIZE], int n);
void printArray(char A[A_SIZE][A_SIZE], char *s);
int letterFrequency(char c,char A[A_SIZE][A_SIZE]);
void mutateArray(char A[A_SIZE][A_SIZE]);
int check(char A[A_SIZE][A_SIZE]);

int main(int argc, char **argv)
{
  int i=0,j=0;
  int letternumber;
  char A[A_SIZE][A_SIZE];
  char c='A';
  if(argc!=2){
    printf("INPUT ERROR\n");
    return -1;
  }
  else{
    letternumber=atoi(argv[1]);
  }

  fillArray(A,letternumber);
  printArray(A ,"Initial Array");
  while(i<letternumber){
    printf("%c=%d ",c,letterFrequency(c,A) );
    c++;
    i++;
  }
  printf("\n");
  while(check(A)!=0){
  mutateArray(A);
  c='A';
  i=0;
  while(i<letternumber){
    printf("%d : %c=%d ",j,c,letterFrequency(c,A) );
    c++;
    i++;
  }
  printf("\n");
  j++;
}
  printArray(A,"Final Array");
  return 0;
}
int check(char A[A_SIZE][A_SIZE])
{
  int i,j,cnt=0;
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      if(A[0][0]!=A[i][j])
        cnt++;
    }
  }
  return cnt;
}
void mutateArray(char A[A_SIZE][A_SIZE])
{
  int x1=0,x2=0,y1=0,y2=0;
  time_t t;
  while(A[y2][x2]==A[y1][x1]){
    srand((unsigned) time(&t)+1);
    x1=rand()%A_SIZE;
    srand((unsigned) time(&t)+2);
    y1=rand()%A_SIZE;
    srand((unsigned) time(&t)+3);
    x2=rand()%A_SIZE;
    srand((unsigned) time(&t)+4);
    y2=rand()%A_SIZE;

  }
  A[y2][x2]=A[y1][x1];
}
void fillArray(char A[A_SIZE][A_SIZE], int n)
{
  int i,j;
  srand(time(NULL));
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      A[i][j]=(rand() % n)+I2CHAR;
    }
  }
}
void printArray(char A[A_SIZE][A_SIZE], char *s)
{
  int i,j;
  printf("%s\n",s );
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      printf("%c", A[i][j]);
    }
    printf("\n");
  }
}
int letterFrequency(char c,char A[A_SIZE][A_SIZE])
{
  int i,j,cnt=0;
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      if(A[i][j]==c){
        cnt++;
      }
    }
  }
  return cnt;
}
