#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define I2CHAR 65
#define LETTER_NUM 3
#define A_SIZE 10

void fillArray(char A[A_SIZE][A_SIZE]);
void printArray(char A[A_SIZE][A_SIZE]);
int letterFrequency(char c,char A[A_SIZE][A_SIZE]);


int main(void)
{
  int i=0;
  char A[A_SIZE][A_SIZE];
  char c='A';
  fillArray(A);
  printArray(A);
  while(i<LETTER_NUM){
    printf("%c=%d ",c,letterFrequency(c,A) );
    c++;
    i++;
  }
  printf("\n");
  return 0;
}
void fillArray(char A[A_SIZE][A_SIZE])
{
  int i,j;
  srand(time(NULL));
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      A[i][j]=(rand() % LETTER_NUM)+I2CHAR;
    }
  }
}
void printArray(char A[A_SIZE][A_SIZE])
{
  int i,j;
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      printf("%c", A[i][j]);
    }
    printf("\n");
  }
}
int letterFrequency(char c,char A[A_SIZE][A_SIZE])
{
  int i,j,cnt=0;
  for(i=0;i<A_SIZE;i++){
    for(j=0;j<A_SIZE;j++){
      if(A[i][j]==c){
        cnt++;
      }
    }
  }
  return cnt;
}
