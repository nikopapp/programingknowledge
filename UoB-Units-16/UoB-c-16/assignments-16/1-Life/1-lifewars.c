/*last version*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define HEIGHT                      90
#define WIDTH                      150
#define TEAM_ONE                     1
#define TEAM_TWO                    10
#define DEAD_CELL                    0
#define H                     HEIGHT-1
#define W                      WIDTH-1
#define AREA              HEIGHT*WIDTH
#define MAX_FILE                   120
#define SLEEP_T               50000000
#define POINTS                     101
#define TEAM_ONE_TWO        2*TEAM_ONE
#define TEAM_ONE_THREE      3*TEAM_ONE
#define TEAM_TWO_TWO        2*TEAM_TWO
#define TEAM_TWO_THREE      3*TEAM_TWO

time_t t;

int sumSpecial(int i, int j, int p[HEIGHT][WIDTH]);
void arrayCopy(int p[HEIGHT][WIDTH], int q[HEIGHT][WIDTH]);
void arraySum(int p [HEIGHT][WIDTH],int a);
int fFillArray(int p[HEIGHT][WIDTH],FILE *fp, int a);
void zFillArray(int p[HEIGHT][WIDTH]);
void nextStep(int p[HEIGHT][WIDTH], int q[HEIGHT][WIDTH], int sum[HEIGHT][WIDTH]);
void mySleep(int a);

void printArray(int p[HEIGHT][WIDTH]);
void printArrayz(int p[HEIGHT][WIDTH]);
void printBoardX(void);

struct life {
  int self[HEIGHT][WIDTH], sum[HEIGHT][WIDTH];
};

int main (int argc, char **argv)
{
   int i=0,iNum;
   struct life a;
   struct life b;
   char *fName1, *fName2, *iter;
   FILE *fp1, *fp2;
   if( argc==4 ){
      fName1=argv[1];
      fName2=argv[2];
      iter=argv[3];
      sscanf(iter, "%d",&iNum);
   }
   else{
      printf("Unable to input you 'arguments'");
      return(0);
   }
   fp1= fopen(fName1, "r");
   fp2= fopen(fName2, "r");
   zFillArray(a.self);
   zFillArray(b.self);
   fFillArray(a.self, fp1, TEAM_ONE);
   fclose(fp1);
   while(i==0){
      i+=fFillArray(a.self, fp2, TEAM_TWO);
      fclose(fp2);
      fp2= fopen(fName2, "r");
      i+=fFillArray(a.self, fp2, TEAM_TWO);
   }
   fclose(fp2);
   i=0;
   while(i<=iNum){
   /*WHERE THE WAR HAPPENS*/
      nextStep(a.self,b.self,b.sum);
      arraySum(b.self,TEAM_ONE);
      arraySum(b.self,TEAM_TWO);
      arrayCopy(b.self,a.self);
      i++;
   }
   return(0);
}
void zFillArray(int p[HEIGHT][WIDTH])
{
   int i,j;
      for(i=0;i<HEIGHT;i++){
         for(j=0;j<WIDTH;j++){
            p[i][j]=0;
         }
      }
}
void arraySum(int p[HEIGHT][WIDTH],int a)
{
  int i,j,sum=0;
  for(i=0;i<HEIGHT;i++){
     for(j=0;j<WIDTH;j++){
        if(p[i][j]==a){
        sum++;
        }
     }
   }
   if(a==TEAM_ONE){
     printf("Team One Score: %d \n", sum);
   }
   if(a==TEAM_TWO){
     printf("Team Two Score: %d \n", sum);
   }
}
int fFillArray(int p[HEIGHT][WIDTH],FILE *fp, int a)
{
   int i,x,y,xOrigin,yOrigin,xFin,yFin;
srand((unsigned) time(&t)+a);
   xOrigin=rand()%WIDTH;
   yOrigin=rand()%HEIGHT;
   for(i=0;i<POINTS;i++){
      if(fscanf(fp,"%d%d",&x,&y)!=2&&i==0){
         printf("FILE ERROR\n");
      }
      xFin=xOrigin+x;
      yFin=yOrigin+y;
      if(xFin>=WIDTH){
         xFin-=W;
      }
      if(yFin>=HEIGHT){
         yFin-=H;
      }
      if(p[yFin][xFin]==DEAD_CELL)
      p[yFin][xFin]=a;
      else if(p[yFin][xFin]!=DEAD_CELL){
        return(1);}
   }
   return(0);
}

void nextStep(int p[HEIGHT][WIDTH], int q[HEIGHT][WIDTH], int sum[HEIGHT][WIDTH])
{
   int j,i;
   for(i=0;i<HEIGHT;i++){
      for(j=0;j<WIDTH;j++){
         sum[i][j] = sumSpecial(i,j,p);
         switch (p[i][j]) {
            case DEAD_CELL:
               switch (sum[i][j]){
                  case TEAM_ONE_THREE:
                     q[i][j]=TEAM_ONE;
                     break;
                  case TEAM_TWO_THREE:
                     q[i][j]=TEAM_TWO;
                     break;
                  default:
                     q[i][j]=DEAD_CELL;
                     break;
               }
               break;
            case TEAM_ONE:
               switch (sum[i][j]){
                  case 2:
                  case 3:
                     q[i][j]=TEAM_ONE;
                     break;
                  default:
                     q[i][j]=DEAD_CELL;
                     break;
               }
               break;
            case TEAM_TWO:
               switch (sum[i][j]){
                  case TEAM_TWO_TWO:
                  case TEAM_TWO_THREE:
                     q[i][j]=TEAM_TWO;
                     break;
                  default:
                     q[i][j]=DEAD_CELL;
                     break;
               }
         }
      }
   }
}
int sumSpecial(int i, int j, int p[HEIGHT][WIDTH])
{
   int sum;
   switch (i) {
      default:
         sum =
         p[i][j+1]+p[i][j-1]+       /*summing cells right-left  */
         p[i+1][j]+p[i-1][j]+       /*summing cells up-down     */
         p[i-1][j+1]+p[i-1][j-1]+   /*summing cells right-left  */
         p[i+1][j+1]+p[i+1][j-1];   /*summing cells right-left  */
         break;
      case 0:
         switch (j) {
            default:
               sum = p[i][j+1]+p[i][j-1]+       /*summing cells right-left  */
               p[i+1][j]+p[H][j]+       /*summing cells up-down     */
               p[H][j+1]+p[H][j-1]+   /*summing cells right-left  */
               p[i+1][j+1]+p[i+1][j-1];   /*summing cells right-left  */
              break;
            case 0:
               sum = p[i][j+1]+p[i][W]+       /*summing cells right-left  */
               p[i+1][j]+p[H][j]+       /*summing cells up-down     */
               p[H][j+1]+p[H][W]+   /*summing cells right-left  */
               p[i+1][j+1]+p[i+1][W];   /*summing cells right-left  */
               break;
            case W:
               sum = p[i][0]+p[i][j-1]+       /*summing cells right-left  */
               p[i+1][j]+p[H][j]+       /*summing cells up-down     */
               p[H][0]+p[H][j-1]+   /*summing cells right-left  */
               p[i+1][0]+p[i+1][j-1];   /*summing cells right-left  */
               break;
        }
         break;
      case H:
      switch (j) {
         default:
           sum =
           p[i][j+1]+p[i][j-1]+       /*summing cells right-left  */
           p[0][j]+p[i-1][j]+       /*summing cells up-down     */
           p[i-1][j+1]+p[i-1][j-1]+   /*summing cells right-left  */
           p[0][j+1]+p[0][j-1];   /*summing cells right-left  */
           break;
        case 0:
            sum =
            p[i][j+1]+p[i][W]+       /*summing cells right-left  */
            p[0][j]+p[i-1][j]+       /*summing cells up-down     */
            p[i-1][j+1]+p[i-1][W]+   /*summing cells right-left  */
            p[0][j+1]+p[0][W];   /*summing cells right-left  */
            break;
         case W:
            sum =
            p[i][0]+p[i][j-1]+       /*summing cells right-left  */
            p[0][j]+p[i-1][j]+       /*summing cells up-down     */
            p[i-1][0]+p[i-1][j-1]+   /*summing cells right-left  */
            p[0][0]+p[0][j-1];   /*summing cells right-left  */
            break;
}
}
   return(sum);
}
void arrayCopy(int p[HEIGHT][WIDTH], int q[HEIGHT][WIDTH])
{
   int i,j;
      for(i=0;i<HEIGHT;i++){
         for(j=0;j<WIDTH;j++){
           q[i][j]=p[i][j];
         }
      }
}
void mySleep(int a)
{
   int i;
      for(i=0;i<a*SLEEP_T;i++){}
}
