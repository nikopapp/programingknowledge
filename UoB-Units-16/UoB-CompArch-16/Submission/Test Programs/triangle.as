LDAC 5    #load triangle height
STAM 110  #store triangle height to memory 110
LDAC 0
STAM 113  #store triangle height to memory 113
LDAC 6    #load triangle base
STAM 111  #store triangle base to memory 111
LDAC 2    #load dividend
STAM 100  #store dividend to memory 100
multiplication:
LDAM 113  #load multi result from memory 113
LDBM 110  #load triangle height from memory 110
ADD
STAM 113  #store multi result to memory 113
LDAM 111  #load multi counter from memory 111
LDBC 1    #load 1 to breg
SUB
STAM 111  #store multi counter to memory 111
BRZ division
BR multiplication
division:
LDAM 113  #load dividend
LDBM 100  #load divisor
SUB
BRN end
STAM 113  #store division temporary rest
LDAM 102  #load division coounter
LDBC 1    #load b to breg
ADD
STAM 102  #store division counter to memory 102
LDAM 113  #load division temporary rest
BRZ end
BR division
end:
LDAM 102 #load triangle area to areg
LDBM 113 #load division rest
BR -2
