/////////////////////////////////////////////////////////////////////////////
//                      _     _
//                     | -1 -1 -1 |
//  Convolution * *    | -1  8 -1 | Sharpen
//                     |_-1 -1 -1_|
//
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "openCV.h"

#define W     512
#define H     512
#define K_SIZE  3
#define MAX_V 255

using namespace cv;

int intSwap(int value);
int invertColor(int value);
void printArray(int A[K_SIZE][K_SIZE]);
int max(int a, int b);

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* outputName = argv[2];
 char* outputName2 = argv[3];
 Mat image;
 image = imread( imageName, 1 );

 if( argc != 4 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }
 /*
 int k1[K_SIZE][K_SIZE];
  for (int i=0;i<K_SIZE;i++){
    for (int j=0;j<K_SIZE;j++){
      k1[i][j]=-1;
    }
  }*/
  int k1[K_SIZE][K_SIZE]{
    {-1,-1,-1},
    {-1,0,-1},
    {-1,-1,-1}
  };
/* int k2[K_SIZE][K_SIZE]{
   {0,0,0},
   {0,9,0},
   {0,0,0}
 };*/
 Mat gray_image;
 Mat sharp_image;
 namedWindow( "ImageWindow", WINDOW_NORMAL );
 printf("Original image displayed...\n");
 imshow("ImageWindow", image);
 //wait till key is pressed
 printf("PRESS A KEY NOW...\n");
 cvWaitKey();
 cvtColor( image, gray_image, CV_BGR2GRAY );
 cvtColor( image, sharp_image, CV_BGR2GRAY );
 imshow("ImageWindow", gray_image);
 printf("Grayscale image displayed...\n");
 //wait till key is pressed
 printf("PRESS A KEY NOW...\n");
 cvWaitKey();


      int tempAnt[K_SIZE][K_SIZE];
  for(int i=1;i<gray_image.rows-1;i+=1){
    for(int j=1;j<gray_image.cols-1;j+=1){
      int accu1=0;
      //int accu2=0;

        //    if(gray_image.at<uchar>(i,j)<200&&gray_image.at<uchar>(i,j)>100){

        for (int a=-1;a<=1;a++){
          for (int b=-1;b<=1;b++){
            tempAnt[a+1][b+1]=gray_image.at<uchar>(i+a,j+b);
            accu1+=(tempAnt[a+1][b+1])*(k1[a+1][b+1]);

        //    printf("%d%d grey %d,  temp %d,  k1 %d, accu %d\n",i,j,
          //  gray_image.at<uchar>(i+a,j+b),tempAnt[a+1][b+1],k1[a+1][b+1],accu1 );

        //    accu2+=(tempAnt[a+1][b+1])*(k1[a+1][b+1])/10;

            }
          }
          printf("\n" );

    //  printf("%4d---%4d\n",accu1,gray_image.at<uchar>(i,j) );


      gray_image.at<uchar>(i,j)=max((int)((8*gray_image.at<uchar>(i,j)+(accu1))/9.0),255);
      //printf("%4d---%4d\n",accu1,gray_image.at<uchar>(i,j) );
  //}


        }


    }
      imshow("ImageWindow", gray_image);
      printf("Retouched image displayed...\n");
      //wait till key is pressed
      printf("PRESS A KEY NOW...\n");
      cvWaitKey();
      imshow("ImageWindow", sharp_image);
      printf("Retouched image displayed...\n");
      //wait till key is pressed
      printf("PRESS A KEY NOW...\n");
      cvWaitKey();
 imwrite( outputName, sharp_image );
 imwrite( outputName2, gray_image );

 return 0;
}

void printArray(int A[K_SIZE][K_SIZE])
{
  for (int a=0;a<K_SIZE;a++){
    for (int b=0;b<K_SIZE;b++){
      printf("%4d",A[a][b] );
    }
    printf("\n" );
  }
}

int intSwap(int value)
{
  if(value>MAX_V/2){
  value=MAX_V/2-value;

  }
  if(value<MAX_V/2){
  value=MAX_V-value;
  }
   return (value);
}
int max(int a, int b)
{
  if(a>b){return a;}
  else{return b;}

}
int invertColor(int value)
{
   value=MAX_V-value;
   return (value);
}
