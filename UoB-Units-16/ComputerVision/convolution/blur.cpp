/////////////////////////////////////////////////////////////////////////////
//                      _     _
//                     | 1 1 1 |
//  Convolution * 1/9 *| 1 1 1 | BLURR
//                     |_1 1 1_|
//
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "openCV.h"

#define W     512
#define H     512
#define K_SIZE  3
#define MAX_V 255

using namespace cv;

int intSwap(int value);
int invertColor(int value);
void printArray(int A[K_SIZE][K_SIZE]);

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* outputName = argv[2];
 Mat image;

 image = imread( imageName, 1 );

 if( argc != 3 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }

 /*int k[K_SIZE][K_SIZE]{
   {-1,0,1},
   {-2,0,2},
   {-1,0,1}
 };*/
int k[K_SIZE][K_SIZE];
 for (int i=0;i<K_SIZE;i++){
   for (int j=0;j<K_SIZE;j++){
     k[i][j]=1;
   }
 }
 Mat gray_image;
 printf("Original image displayed...\n");
 imshow("ImageWindow", image);
 cvtColor( image, gray_image, CV_BGR2GRAY );
 //wait till key is pressed
 printf("PRESS A KEY NOW...\n");
 cvWaitKey();

 imshow("ImageWindow", gray_image);
 printf("Retouched image displayed...\n");
 //wait till key is pressed
 printf("PRESS A KEY NOW...\n");
 cvWaitKey();

      for(int r=0;r<=1;r++){
      int tempAnt[K_SIZE][K_SIZE];
  for(int i=1;i<gray_image.rows-1;i+=1){
    for(int j=1;j<gray_image.cols-1;j+=1){
      int accu=0;

        for (int a=-1;a<=1;a++){
          for (int b=-1;b<=1;b++){
            tempAnt[a+1][b+1]=gray_image.at<uchar>(i+a,j+b);
            accu+=(tempAnt[a+1][b+1])*(k[a+1][b+1])/9;
            }
          }

      gray_image.at<uchar>(i,j)=accu;
        }
      }
      imshow("ImageWindow", gray_image);
      printf("Retouched image displayed...\n");
      //wait till key is pressed
      printf("PRESS A KEY NOW...\n");
      cvWaitKey();

    }
 imwrite( outputName, gray_image );

 return 0;
}

void printArray(int A[K_SIZE][K_SIZE])
{
  for (int a=0;a<K_SIZE;a++){
    for (int b=0;b<K_SIZE;b++){
      printf("%4d",A[a][b] );
    }
    printf("\n" );
  }
}

int intSwap(int value)
{
  if(value>MAX_V/2){
  value=MAX_V/2-value;

  }
  if(value<MAX_V/2){
  value=MAX_V-value;
  }
   return (value);
}

int invertColor(int value)
{
   value=MAX_V-value;
   return (value);
}
