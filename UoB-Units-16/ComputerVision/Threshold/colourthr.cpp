/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - colourthr.cpp
// TOPIC: basic image operations
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* imageOutName = argv[2];
 int thres = atoi(argv[3]);
 int limiter = atoi(argv[4]);

 Mat image;
 image = imread( imageName);

 if( argc != 5 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }

 // THRESHOLD BY LOOPING THROUGH ALL PIXELS
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {

     uchar pixelBlue = image.at<Vec3b>(i,j)[0];/*
     uchar pixelGreen = image.at<Vec3b>(i,j)[1];
     uchar pixelRed = image.at<Vec3b>(i,j)[2];*/

     if (pixelBlue>thres) {
       image.at<Vec3b>(i,j)[0]=limiter;

     }
     else {
       image.at<Vec3b>(i,j)[1]=0;
       image.at<Vec3b>(i,j)[2]=0;
/*
       image.at<Vec3b>(i,j)[0]=0;
     */}
   }
 }
  imwrite( imageOutName, image );

 return 0;
}
