/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - RGBtoHSV.cpp
// TOPIC: basic image operations
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include "openCV.h"
using namespace cv;

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* outName = argv[2];

 Mat image;
 image = imread( imageName, 1 );

 if( argc != 3 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }

 cvtColor( image, image, CV_BGR2HSV );
 imwrite( outName, image );

 return 0;
}
