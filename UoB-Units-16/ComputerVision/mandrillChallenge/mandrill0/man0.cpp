/////////////////////////////////////////////////////////////////////////////
//
//       Process  swap R--G
//                     G--B
//                     B--R
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include "../openCV.h"

using namespace cv;

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* outputName = argv[2];

 Mat image;
 image = imread( imageName, 1 );

 if( argc != 3 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }
int Temp[image.rows][image.cols];

 // THRESHOLD BY LOOPING THROUGH ALL PIXELS
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {
       uchar pixelBlue = image.at<Vec3b>(i,j)[0];
       uchar pixelGreen = image.at<Vec3b>(i,j)[1];
       uchar pixelRed = image.at<Vec3b>(i,j)[2];
   }
 }
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {
      Temp[i][j]=image.at<Vec3b>(i,j)[0];
      image.at<Vec3b>(i,j)[0]=image.at<Vec3b>(i,j)[2];
      image.at<Vec3b>(i,j)[2]=image.at<Vec3b>(i,j)[1];
      image.at<Vec3b>(i,j)[1]=Temp[i][j];


//red=blue
//image.at<Vec3b>(i,j)[2]=image.at<Vec3b>(i,j)[0];
   }
 }
 imwrite( outputName, image );

 return 0;
}
