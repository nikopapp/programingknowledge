/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - threshold.cpp
// TOPIC: basic image operations
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include "/usr/include/opencv2/opencv.hpp"
#include "/usr/include/opencv2/core/core.hpp"
#include "/usr/include/opencv2/highgui/highgui.hpp"

using namespace cv;

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];

 Mat image;
 image = imread( imageName, 1 );

 if( argc != 2 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }

 // CONVERT COLOUR AND SAVE
 Mat gray_image;
 cvtColor( image, gray_image, CV_BGR2GRAY );

 // THRESHOLD BY LOOPING THROUGH ALL PIXELS
 for(int i=0; i<gray_image.rows; i++) {
   for(int j=0; j<gray_image.cols; j++) {
     uchar pixel = gray_image.at<uchar>(i,j);
     if (pixel>128) gray_image.at<uchar>(i,j)=255;
     else gray_image.at<uchar>(i,j)=0;
   }
 }

 imwrite( "threshold.jpg", gray_image );

 return 0;
}
