/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - colourthr.cpp
// TOPIC: basic image operations
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include "openCV.h"

using namespace cv;

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* outputName = argv[2];

 Mat image;
 image = imread( imageName, 1 );

 if( argc != 3 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }

 // THRESHOLD BY LOOPING THROUGH ALL PIXELS
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {

     uchar pixelBlue = image.at<Vec3b>(i,j)[0];
     uchar pixelGreen = image.at<Vec3b>(i,j)[1];
     uchar pixelRed = image.at<Vec3b>(i,j)[2];

     if (pixelBlue>200) {
       image.at<Vec3b>(i,j)[0]=255;
       image.at<Vec3b>(i,j)[1]=255;
       image.at<Vec3b>(i,j)[2]=255;
     }
     else {
       image.at<Vec3b>(i,j)[0]=0;
       image.at<Vec3b>(i,j)[1]=0;
       image.at<Vec3b>(i,j)[2]=0;
     }
   }
 }

 imwrite( outputName, image );

 return 0;
}
