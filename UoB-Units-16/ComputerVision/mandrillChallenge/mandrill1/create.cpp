/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - create.cpp
// TOPIC: basic image operations
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include "../openCV.h"

using namespace cv;

int main( int argc, char** argv )
{

Mat image = cv::Mat(512, 512, CV_8UC3);

 // LOOPING THROUGH ALL PIXELS AND CREATE SOME PIXEL PATTERN
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {

     image.at<Vec3b>(i,j)[0] = i%256;
     image.at<Vec3b>(i,j)[1] = j%256;
     image.at<Vec3b>(i,j)[2] = 0;
   }
 }
 cvShowImage("ImageWindow", myImage);
 printf("Image displayed...\n");
 //imwrite( "test.jpg", image );

 return 0;
}
