/////////////////////////////////////////////////////////////////////////////
//
//       Process  translates red X Y
//
//
//
/////////////////////////////////////////////////////////////////////////////

// header inclusion
#include <stdio.h>
#include <stdlib.h>
#include "../openCV.h"

#define X 35
#define Y 35

#define W 512
#define H 512

using namespace cv;

int overspill(int coordinate, int max);

int main( int argc, char** argv )
{

 // LOADING THE IMAGE
 char* imageName = argv[1];
 char* outputName = argv[2];
 int a=0,b=0;
 Mat image;
 image = imread( imageName, 1 );
 int temp[image.rows][image.cols];

 if( argc != 3 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }
 // THRESHOLD BY LOOPING THROUGH ALL PIXELS
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {
       uchar pixelBlue = image.at<Vec3b>(i,j)[0];
       uchar pixelGreen = image.at<Vec3b>(i,j)[1];
       uchar pixelRed = image.at<Vec3b>(i,j)[2];
   }
 }
 for(int i=0; i<image.rows; i++) {
   for(int j=0; j<image.cols; j++) {
     a = overspill(i-X,image.rows);
     b = overspill(j-Y,image.cols);

     temp[i][j]=image.at<Vec3b>(a,b)[2];



//red=blue
//image.at<Vec3b>(i,j)[2]=image.at<Vec3b>(i,j)[0];
   }
 }

    for(int j=0; j<image.cols; j++) {
       for(int i=0; i<X; i++){
         image.at<Vec3b>(i,j)[2]=60+rand()%120;

       }
       for(int i=X; i<image.rows; i++) {
          image.at<Vec3b>(i,j)[2]=temp[i][j];
       }
     }
 imshow("ImageWindow", image);
 printf("Image displayed...\n");
 //wait till key is pressed
 printf("PRESS A KEY NOW...\n");
 cvWaitKey();
 imwrite( outputName, image );

 return 0;
}
int overspill(int coordinate, int max)
{
   int division;
   if (coordinate>(max-1)){
      division = coordinate/(max-1);
      coordinate -= (division*(max));
   }
   if (coordinate < 0){
     coordinate += (max);
   }
   return (coordinate);
}
